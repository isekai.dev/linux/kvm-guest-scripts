#!/usr/bin/zsh

## Dependencies:
## - libguestfs:
##   - Website: http://libguestfs.org/
##   - AUR Package: https://aur.archlinux.org/packages/libguestfs
##
## Fetches the usage of disks attached to guest machines, and then writes the values to /tmp using
## the following heirarchy:
##
## tmp/
## └── guest-disk-usage/
##     ├── guest-a/
##     │   ├── sda1/
##     │   │   ├── free
##     │   │   ├── percentage
##     │   │   ├── size
##     │   │   └── used
##     │   └── sdb1/
##     │       ├── free
##     │       ├── percentage
##     │       ├── size
##     │       └── used
##     └── guest-b/
##         └── sdc1/
##             ├── free
##             ├── percentage
##             ├── size
##             └── used
##
## This is intended to be automatically run every x minutes, for use in a status display tool such
## as Conky; the virt-df command is not cheap (in both CPU usage and time), so it is preferred to
## run it exactly once per update and interpret the cached results for each required metric, rather
## than run the command for every metric required.

## Get raw data from virt-df.
raw=$(virt-df --human-readable)

## Get list of guest disks.
disks=($(egrep --only-matching '[a-zA-Z0-9]+:/dev/[a-z0-9]+' <<< $raw))

for disk in $disks; do
    ## Get guest name.
    guest=$(cut --delimiter : --fields 1 <<< $disk)

    ## Get disk identifier.
    diskid=$(cut --delimiter : --fields 2 <<< $disk | cut --delimiter / --fields 3)

    ## Ensure directory exists.
    mkdir --parents /tmp/guest-disk-usage/$guest/$diskid

    ## Get all fields for this disk.
    allfields=$(grep $disk <<< $raw)

    ## Get size, used, and available fields for this disk.
    fields=($(egrep --only-matching '[0-9]{1,3}[A-Z]|[0-9][.][0-9][A-Z]' <<< $allfields))

    ## Get percentage field for this disk.
    percentage=$(egrep --only-matching '[0-9]{1,3}%' <<< $allfields | cut --delimiter % --fields 1)

    ## Write results to files.
    cat <<< $fields[1] > /tmp/guest-disk-usage/$guest/$diskid/size
    cat <<< $fields[2] > /tmp/guest-disk-usage/$guest/$diskid/used
    cat <<< $fields[3] > /tmp/guest-disk-usage/$guest/$diskid/free
    cat <<< $percentage > /tmp/guest-disk-usage/$guest/$diskid/percentage
done
