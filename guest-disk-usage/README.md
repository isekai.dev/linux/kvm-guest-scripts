## Details

This script will retrieve disk space information for all KVM/QEMU virtual machines on the system (either under `qemu:///system` or `qemu:///user`, depending on who / how it is run). After retrieving disk space information, it will write the data to `/tmp` within the following heirarchy:

> ```
> /tmp/
> └── guest-disk-usage/
>     ├── guest-a/
>     │   ├── sda1/
>     │   │   ├── free
>     │   │   ├── percentage
>     │   │   ├── size
>     │   │   └── used
>     │   └── sdb1/
>     │       ├── free
>     │       ├── percentage
>     │       ├── size
>     │       └── used
>     └── guest-b/
>         └── sdc1/
>             ├── free
>             ├── percentage
>             ├── size
>             └── used
> ```

Each file contains the metric value - with units in the case of `free`, `size`, and `used` (`percentage` has no unit, nor the `%`).

The original use-case for this script was to have this run every minute and save the metrics somewhere so that Conky could access the values, without needing to run `virt-df` (which is both CPU- and time-expensive) multiple times per update.

## Dependencies

libguestfs: http://libguestfs.org/
- Arch Linux (AUR) Package: https://aur.archlinux.org/packages/libguestfs

## Installation

To install the script into the system for the aforementioned use case:

```zsh
## Copy the script into place.
cp ./guest-disk-usage.zsh /usr/local/bin/guest-disk-usage.zsh

## Make the script executable.
chmod u+x /usr/local/bin/guest/disk-usage.zsh

## Copy the systemd files into place.
cp ./guest-disk-usage.service   /etc/systemd/system/guest-disk-usage.service
cp ./guest-disk-usage.timer     /etc/systemd/system/guest-disk-usage.timer

## Enable and start the systemd timer for the service.
systemctl enable --now guest-disk-usage.timer
```
